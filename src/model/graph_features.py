from itertools import count

import networkx as nx
import pandas as pd

from setup.database import db_con


db = db_con()


def make_nx_graph():
    """
    Makes networkx graph from CSV with information on all nodes
    Info: Site ID, type of node, downstream nodes, storm tank, inflow, redacted XY coordinates, lat/lon coordinates, spill levels (mm)
    """
    df = pd.DataFrame(db["WESSEX_Site_Info"].find({}, {"_id": 0}))
    DG = nx.DiGraph()
    for i, row in df.iterrows():
        site_id = row["Site_ID"]
        DG.add_node(
            site_id,
            node_type=row["type"],
            storm_tank=row["storm_tank"],
            inflow=row["inflow"],
            x=row["X_redacted"],
            y=row["Y_redacted"],
        )
        if row["down_stream"] != "0":
            DG.add_edge(
                site_id, row["down_stream"],
            )
    return DG


def compare_edges(node):
    DG = make_nx_graph()
    node_zero = len(DG.in_edges(node[0])) + len(DG.out_edges(node[0]))
    node_one = len(DG.in_edges(node[1])) + len(DG.out_edges(node[1]))
    if node_zero > node_one:
        return node[0]
    elif node_one > node_zero:
        return node[1]
    elif node_one == node_zero:
        return node[1]


def make_nx_graph():
    """
    Makes networkx graph from CSV with information on all nodes
    Info: Site ID, type of node, downstream nodes, storm tank, inflow, redacted XY coordinates, lat/lon coordinates, spill levels (mm)
    """
    df = pd.DataFrame(db["WESSEX_Site_Info"].find({}, {"_id": 0}))
    DG = nx.DiGraph()
    for i, row in df.iterrows():
        site_id = row["Site_ID"]
        DG.add_node(
            site_id,
            node_type=row["type"],
            storm_tank=row["storm_tank"],
            inflow=row["inflow"],
            x=row["X_redacted"],
            y=row["Y_redacted"],
        )
        if row["down_stream"] != "0":
            DG.add_edge(
                site_id, row["down_stream"],
            )
    return DG


def check_connection(nodes):
    DG = make_nx_graph()
    if nx.has_path(DG, nodes[0], nodes[1]):
        return nx.shortest_path_length(DG, nodes[0], nodes[1])
    elif nx.has_path(DG, nodes[1], nodes[0]):
        return nx.shortest_path_length(DG, nodes[1], nodes[0])
    else:
        return "-"
