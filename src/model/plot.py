from typing import Tuple

import matplotlib
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import pandas as pd
import plotly
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from scipy.spatial.distance import cdist
from tslearn import metrics

from setup.database import db_con
from model.features import get_cso_cluster
from model.graph_features import make_nx_graph

db = db_con()


def draw_correlation_table(
    data_select: str, connected: str, metric: str, cutoff: float
) -> Tuple[pd.DataFrame, np.ndarray]:
    """Creates dataframe of pairwise correlations"""
    if data_select == "60m":
        df = pd.DataFrame(db["kk_sensor_correlation_60m"].find({}, {"_id": 0}))
    elif data_select == "15m":
        df = pd.DataFrame(db["kk_sensor_correlation_15m"].find({}, {"_id": 0}))
    if connected == "Connected":
        df = df[df["Connect"].apply(lambda x: type(x) == int)]

    df = df.sort_values(metric, ascending=False)
    df = df[df[metric] > cutoff]
    print(type(df))
    print(type(df["Min Connect"].unique()))
    return df, df["Min Connect"].unique()


def plot_cso_levels(df: pd.DataFrame, cso_vis_list: str) -> matplotlib.figure.Figure:
    if len(cso_vis_list) == 0:
        return {}
    fig = make_subplots(
        rows=len(cso_vis_list),
        cols=1,
        subplot_titles=cso_vis_list,
        shared_xaxes=True,
        vertical_spacing=0.1,
    )
    for i in range(len(cso_vis_list)):
        fig.add_trace(
            go.Scatter(x=df.index, y=df[cso_vis_list[i]], name=cso_vis_list[i]),
            row=i + 1,
            col=1,
        )
    fig.update_layout(
        margin=dict(l=20, r=20, b=0, t=20),
        height=len(cso_vis_list) * 172,
        showlegend=False,
    )
    return fig


def plot_map_with_proposed(
    df_corr: pd.DataFrame, metric: str, cutoff: float, cso_select: str
) -> plotly.graph_objects.Figure:
    df = pd.DataFrame(db["WESSEX_Site_Info"].find({}, {"_id": 0}))
    nodes_to_remove = list(set(df_corr[df_corr[metric] > cutoff]["Min Connect"]))
    df["Label"] = np.where(
        df["Site_ID"].isin(nodes_to_remove) == True, "Remove", "Maintain"
    )
    for cso in cso_select:
        df.loc[(df.iloc[:, df.columns.get_loc("Site_ID")] == cso), "Label"] = "Selected"
    fig = px.scatter_mapbox(
        df,
        lat="lat",
        lon="lon",
        hover_name="Site_ID",
        color="Label",
        color_discrete_map={
            "Remove": "teal",
            "Maintain": "lightgrey",
            "Selected": "red",
        },
        zoom=10,
        height=306,
        width=1400,
    )
    fig.update_layout(
        mapbox_style="carto-positron",
        margin={"r": 0, "t": 0, "l": 0, "b": 0},
        showlegend=True,
        legend_title_text="",
        legend=dict(
            x=0,
            y=0.10,
            traceorder="reversed",
            orientation="h",
            bgcolor="rgba(0,0,0,0)",
            font=dict(size=13),
        ),
    )
    return fig


def plot_network(target: str) -> plotly.graph_objects.Figure:
    upstream, downstream, cso_cluster = get_cso_cluster(target)
    DG = make_nx_graph()

    node_x = []
    node_y = []
    node_types = []
    for node in DG.nodes():
        x, y = DG.nodes[node]["x"], DG.nodes[node]["y"]
        node_x.append(x)
        node_y.append(y)
        if node in cso_cluster:
            node_type = 1
            node_types.append(node_type)
        else:
            node_type = 0.5
            node_types.append(node_type)

    node_info = []
    for node in DG.nodes(True):
        node_data = []
        node_data.append(node[0])
        node_data.append(node[1]["node_type"])
        node_info.append(node_data)
    nodes_list = np.array(DG.nodes).tolist()
    text_list = [node if node in cso_cluster else "" for node in nodes_list]
    color = [
        0
        if v == "CSO"
        else 1
        if v == "PumpingStation"
        else 2
        if v == "WaterRecyclingCentre"
        else 3
        for v in list(nx.get_node_attributes(DG, "node_type").values())
    ]

    node_trace = go.Scatter(
        x=node_x,
        y=node_y,
        mode="markers+text",
        hovertext=node_info,
        hoverinfo="text",
        marker=dict(
            color=color,
            colorscale=px.colors.qualitative.D3,
            opacity=node_types,
            size=10,
        ),
        text=text_list,
        textposition="top center",
        textfont_size=15,
    )

    fig = go.Figure(
        data=node_trace,
        layout=go.Layout(
            margin=dict(b=0, l=0, r=0, t=0),
            xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
            yaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
        ),
    )

    # arrows rather than lines required annotations
    edge_start = []
    edge_end = []
    for edge in DG.edges():
        x0, y0 = DG.nodes[edge[0]]["x"], DG.nodes[edge[0]]["y"]
        x1, y1 = DG.nodes[edge[1]]["x"], DG.nodes[edge[1]]["y"]
        edge_start.append([x0, y0])
        edge_end.append([x1, y1])

    for i in range(len(edge_start)):
        fig.add_annotation(
            ax=edge_start[i][0],  # arrows' tail
            ay=edge_start[i][1],  # arrows' tail
            x=edge_end[i][0],  # arrows' head
            y=edge_end[i][1],  # arrows' head
            xref="x",
            yref="y",
            axref="x",
            ayref="y",
            text="",  # if you want only the arrow
            showarrow=True,
            arrowhead=2,
            arrowsize=2,
            arrowwidth=1,
            arrowcolor="orange",
            opacity=0.7,
        )

    # to set appropriately zoomed in axis on cluster
    cluster_x = []
    cluster_y = []
    for node in cso_cluster:
        cluster_x.append(DG.nodes[node]["x"])
        cluster_y.append(DG.nodes[node]["y"])

    axis_buffer = 250
    fig.update_xaxes(
        range=[min(cluster_x) - axis_buffer, max(cluster_x) + axis_buffer],
        showgrid=False,
    )
    fig.update_yaxes(
        range=[min(cluster_y) - axis_buffer, max(cluster_y) + axis_buffer],
        showgrid=False,
    )

    fig.update_layout(
        height=195,
        width=600,
    )
    return fig


def get_dtw_cost_matrix(compare_2, comparing):
    """Gets the cost matrix between 2 timeseries using matplotlib
    compare_2 - timeseries formatted to ts learn and scaled
    comparing - list of strings giving 2 nodes being compared
    retunrs - matplotlib fig
    """
    s_y1 = compare_2[0]
    s_y2 = compare_2[1]
    sz = s_y1.shape[0]

    path, sim = metrics.dtw_path(s_y1, s_y2)

    plt.figure(1, figsize=(8, 8))

    # definitions for the axes
    left, bottom = 0.01, 0.1
    w_ts = h_ts = 0.2
    left_h = left + w_ts + 0.02
    width = height = 0.65
    bottom_h = bottom + height + 0.02

    rect_s_y = [left, bottom, w_ts, height]
    rect_gram = [left_h, bottom, width, height]
    rect_s_x = [left_h, bottom_h, width, h_ts]

    ax_gram = plt.axes(rect_gram)
    ax_s_x = plt.axes(rect_s_x)
    ax_s_y = plt.axes(rect_s_y)

    mat = cdist(s_y1, s_y2)

    ax_gram.imshow(mat, origin="lower")
    ax_gram.axis("off")
    ax_gram.autoscale(False)
    ax_gram.plot([j for (i, j) in path], [i for (i, j) in path], "w-", linewidth=2.0)

    ax_s_x.plot(np.arange(sz), s_y2, "b-", linewidth=2.0)
    ax_s_x.axis("off")
    ax_s_x.set_xlim((0, sz - 1))
    ax_s_x.set_title(f"{comparing[0]}")

    ax_s_y.plot(-s_y1, np.arange(sz), linewidth=2.0, color="red")
    ax_s_y.axis("off")
    ax_s_y.set_ylim((0, sz - 1))
    ax_s_y.set_title(f"{comparing[1]}")

    plt.tight_layout()

    return plt
