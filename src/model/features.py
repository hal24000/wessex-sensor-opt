import itertools

import geopy.distance
import pandas as pd
from scipy.stats import kendalltau, pearsonr, spearmanr
from tslearn.metrics import dtw
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from tslearn.utils import to_time_series_dataset

from model.graph_features import check_connection, compare_edges
from setup.database import db_con


db = db_con()

def calculate_corr(df, resample_time, calculate_dtw):
    """
    Calculates correlation values for all combinations of sensors
    Args:
        df: Data
        resample_time: Resampling time of data
        calculate_dtw: Whether to calculate DTW (takes several hours to calculate)

    """
    df_diff = df.diff().dropna()
    res = pd.DataFrame()
    cols = df.columns.to_list()
    df_list = [df, df_diff]
    functions_list = [pearsonr, spearmanr, kendalltau]
    for dataframe in df_list:
        for func in functions_list:
            corr_dict = {}
            for col_a, col_b in itertools.combinations(cols, 2):
                corr_dict[col_a, col_b] = func(
                    dataframe.loc[:, col_a], dataframe.loc[:, col_b]
                )
            res_temp = pd.DataFrame.from_dict(corr_dict, orient="index")
            res = pd.concat([res, res_temp], axis=1)

    res.columns = [
        "Pearson",
        "pvalue Pearson",
        "Spearman",
        "pvalue Spearman",
        "Kendall",
        "pvalue Kendall",
        "Diff Pearson",
        "pvalue Diff Pearson",
        "Diff Spearman",
        "pvalue Diff Spearman",
        "Diff Kendall",
        "pvalue Diff Kendall",
    ]
    res = res.sort_values("Pearson", ascending=False)
    res.index.names = ["Nodes"]
    res = res.reset_index()

    res["Connect"] = res["Nodes"].apply(check_connection)
    res["Dist (km)"] = res["Nodes"].apply(calculate_node_distance)
    res["CSO 1"] = res["Nodes"].apply(lambda x: x[0])
    res["CSO 2"] = res["Nodes"].apply(lambda x: x[1])
    res["Min Connect"] = res["Nodes"].apply(compare_edges)

    if resample_time == "60m":
        collection = "kk_dtw"
    elif resample_time == "15m":
        collection = "kk_dtw_15m"

    if calculate_dtw:
        dtw_series = res.apply(lambda row: calculate_dtw_score(df, row), axis=1)
        res["DTW"] = dtw_series
        db[collection].drop()
        db[collection].insert_many(res.to_dict(orient="records"))

    res["DTW"] = pd.DataFrame(db[collection].find({}, {"_id": 0, "DTW": 1}))
    res = round(res, 2)
    res = res[
        [
            "CSO 1",
            "CSO 2",
            "Connect",
            "Dist (km)",
            "Diff Pearson",
            "Diff Spearman",
            "DTW",
            "Min Connect",
        ]
    ]
    res = res.rename(columns={"Diff Pearson": "Pearson", "Diff Spearman": "Spearman",})
    res = res.sort_values("Pearson", ascending=False)
    res = res.reset_index(drop=True)
    print("Dataframe generated.")
    return res


def calculate_dtw_score(df, row):
    cso_1 = row["CSO 1"]
    cso_2 = row["CSO 2"]
    transformed_data = to_time_series_dataset([df[j].values for j in [cso_1, cso_2]])
    dtw_score = dtw(transformed_data[0], transformed_data[1])
    return dtw_score


def calculate_node_distance(nodes):
    query = {}
    project = {"_id": 0, "Site_ID": 1, "lat": 1, "lon": 1}
    lat_lon_df = pd.DataFrame(db["WESSEX_Site_Info"].find(query, project))
    lat_dict = dict(zip(lat_lon_df["Site_ID"], lat_lon_df["lat"]))
    lon_dict = dict(zip(lat_lon_df["Site_ID"], lat_lon_df["lon"]))

    first_node = (lat_dict[nodes[0]], lon_dict[nodes[0]])
    second_node = (lat_dict[nodes[1]], lon_dict[nodes[1]])
    distance = geopy.distance.geodesic(first_node, second_node).km
    distance = round(distance, 2)
    return distance


def get_cso_cluster(target):
    """
    For a given CSO, returns the upstream, downstream CSOs and total list of CSOs

    Args:
        target (str): Target CSO

    Returns:
        upstream (str): Upstream CSO
        downtream (str): Downstream CSO
        cso_cluster (list[str]): List of target, upstream, downstream CSOs

    """
    df = pd.DataFrame(
        db["WESSEX_Site_Info"].find({}, {"_id": 0, "Site_ID": 1, "down_stream": 1})
    )

    try:
        upstream = df[df["down_stream"] == target]["Site_ID"].values.tolist()
        downstream = df[df["Site_ID"] == target]["down_stream"].values.tolist()
        cso_chain = [[target], upstream, downstream]
    except IndexError:
        try:
            upstream = "No upstream CSO exists."
            downstream = df[df["Site_ID"] == target]["down_stream"].values.tolist()
            cso_chain = [[target], downstream]
        except IndexError:
            upstream = df[df["down_stream"] == target]["Site_ID"].values.tolist()
            downstream = "No downstream CSO exists."
            cso_chain = [[target], upstream]

    cso_cluster = [item for sublist in cso_chain for item in sublist]

    sites_with_data, sites_with_no_data = get_sites()
    cso_cluster = [x for x in cso_cluster if x not in sites_with_no_data]
    upstream = [x for x in upstream if x not in sites_with_no_data]
    downstream = [x for x in downstream if x not in sites_with_no_data]
    return upstream, downstream, cso_cluster


def get_levels(resample_time, cso1=None, cso2=None):
    if resample_time == "60m":
        collection = "WESSEX_E_Numbers_Apr_2019_60Min_Mean"
    elif resample_time == "15m":
        collection = "WESSEX_E_Numbers_Apr_2019_15Min_Mean"
    query = {}
    if cso1 == None:
        project = {"_id": 0}
    else:
        project = {"_id": 0, "Datetime": 1, cso1: 1, cso2: 1}
    df = (
        pd.DataFrame(db[collection].find(query, project))
        .set_index("Datetime")
        .sort_index()
    )
    return df


def get_sites():
    """
    Find the sites that have level data available i.e. not a WRC or pumping station.

    Args:
        None

    Returns:
        sites_with_data (list[str]): List of CSOs that have data
        sites_with_no_data (list[str]): List of CSOs that have no data

    """
    site_info = pd.DataFrame(db["WESSEX_Site_Info"].find({}, {"_id": 0, "Site_ID": 1}))
    levels = pd.DataFrame(
        db["WESSEX_E_Numbers_Apr_2019_60Min_Mean"].find_one({}, {"_id": 0}), index=[0]
    ).set_index("Datetime")
    sites_with_no_data = list(set(site_info["Site_ID"]) - set(levels.columns))
    sites_with_data = [x for x in levels.columns if x not in sites_with_no_data]
    return sites_with_data, sites_with_no_data


def ingest_corr_df(
    resample_time, difference, scaler_method, calculate_dtw, output_collection
):
    """
    "kk_sensor_correlation_60m"
    "kk_sensor_correlation_15m"

    Args:
        resample_time
        difference
        scaler_method
        calculate_dtw
        output_collection
    """
    df = get_levels(resample_time)
    if difference:
        df = df.diff().dropna()
    df_norm = normalise_levels(df, scaler_method)
    df_corr = calculate_corr(df_norm, resample_time, calculate_dtw)
    db[output_collection].drop()
    db[output_collection].insert_many(df_corr.to_dict(orient="records"))
    print(f"Ingestion into {output_collection} complete.")


def normalise_levels(df, method):
    res_df = df.copy()
    if method == "minmax":
        scaler_method = MinMaxScaler()
    elif method == "standard":
        scaler_method = StandardScaler()

    for col in df.columns:
        scaler = scaler_method
        res_df[col] = scaler.fit_transform(res_df[col].values.reshape(-1, 1))
    res_df = res_df.dropna(axis=0)
    return res_df
