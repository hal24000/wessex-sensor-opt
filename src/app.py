import streamlit as st

from pages.page_dashboard import run_dashboard
from setup.layout import setup_page


setup_page("Sensor Correlation")
run_dashboard()

with st.sidebar:
    st.header("")
    st.info(
        """
    ### :information_source: Info
    - Statistical techniques used to compare sensor levels.
    - For high correlations, a case can be made to remove the least connected sensor.
    """
    )
