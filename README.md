# Sensor Placement Optimisation

Statistical analysis of network to reduce the number of necessary sensors.

## Usage
```bash
$ cd wessex-sensor-opt/src
$ streamlit run app.py
```

## Project Organisation

    ├── chart                        <- Files required for Dimensionops build
    │
    ├── notebooks                    <- Jupyter notebooks
    │
    ├── src                          <- Source code for project
    │   │
    │   │── model                    <- Model code
    │   │   │── features.py          <- Feature generation
    │   │   │── graph_features.py    <- Graph feature generation
    │   │   └── plot.py              <- Visualization plots
    │   │
    │   │── pages                    <- App pages
    │   │   └── page_dashboard.py    <- Main dashboard
    │   │
    │   │── setup                    <- Basic setup files
    │   │   │── database.py          <- Database connection
    │   │   │── favicon.ico          <- Icon
    │   │   └── layout.py            <- Layout settings
    │   │
    │   └── app.py                   <- App run file
    │   
    │── Dockerfile                   <- File to assemble a Docker image
    │
    │── environment.yml              <- Environment yml file to create conda environment
    │
    ├── README.md                    <- README for this project
    │
    └── requirements.txt             <- Requirements file for creating app environment